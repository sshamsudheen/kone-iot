module.exports = {
    getHomePage: (req, res) => {
        let query = "SELECT * FROM `equipments` ORDER BY id ASC"; // query database to get all the equipments

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('index.ejs', {
                title:" Welcome to Samsudheen-IoT |View Equipments"
                ,players: result
            });
        });
    },

    searchEquipmentPage: (req, res) => {
        let query = "SELECT * FROM `equipments` ORDER BY id ASC"; // query database to get all the equipments

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('index.ejs', {
                title:" View Equipments"
                ,players: result
            });
        });
    },
};
