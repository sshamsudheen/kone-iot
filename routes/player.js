const fs = require('fs');

module.exports = {
    addEquipmentPage: (req, res) => {
        res.render('add-equipment.ejs', {
            title: "Welcome to Samsudheen-IoT | Add a new Equipment"
            ,message: ''
        });
    },
    addEquipment: (req, res) => {


        let message = '';


        let address = req.body.address;
        let equipment_id = req.body.equipment_id;
        let status = req.body.status;
        let contract_start_date = req.body.contract_start_date;
        let contract_end_date = req.body.contract_end_date;

        let usernameQuery = "SELECT * FROM `equipments` WHERE equipment_id = '" + equipment_id + "'";

        db.query(usernameQuery, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            if (result.length > 0) {
                message = 'Equipmet_id already exists';
                res.render('add-equipment.ejs', {
                    message,
                    title: " Welcome to Samsudheen-IoT | Add a new Equipment"
                });
            } else {
                if (equipment_id !== "") {


                        let query = "INSERT INTO `equipments` (equipment_id, address, status, contract_start_date, contract_end_date ) VALUES ('" +
                            equipment_id + "', '" + address + "', '" + status + "', '" + contract_start_date + "', '" + contract_end_date + "' )";
                        db.query(query, (err, result) => {
                            if (err) {
                                return res.status(500).send(err);
                            }
                            res.redirect('/');
                        });

                } else {
                    message = "equipment id should be entered";
                    res.render('add-equipment.ejs', {
                        message,
                        title: " Welcome to Samsudheen-IoT | Add a new Equipment"
                    });
                }
            }
        });
    },
    viewEquipment: (req, res) => {
        let playerId = req.params.id;
        let query = "SELECT * FROM `equipments` WHERE equipment_id = '" + playerId + "' ";
        db.query(query, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.render('edit-equipment.ejs', {
                title: "Welcome to Samsudheen-IoT | View Equipment"
                ,player: result[0]
                ,message: ''
            });
        });
    },
    editEquipment: (req, res) => {
        let playerId = req.params.id;
        let address = req.body.address;
        let status = req.body.status;


        let query = "UPDATE `equipments` SET `address` = '" + address + "', `status` = '" + status + "' WHERE `equipments`.`id` = '" + playerId + "'";
        db.query(query, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.redirect('/');
        });
    },





    listEquipment: (req, res) => {
        let eqpId = req.params.id;
        let query = 'SELECT * from `equipments`  limit  ' + eqpId + '';

        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('index.ejs', {
                title:" Welcome to Samsudheen-IoT | View Equipments"
                ,players: result
            });
        });
    }
};
